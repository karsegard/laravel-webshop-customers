<div class="container">
    <div class="row justify-content-center">
        @if(!kda_webshop_auth()->user()->hasVerifiedEmail())
            <div>
                Votre compte n'est pas vérifié
            </div>
        @endif

        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}


                    <a href="/shop/account/addresses">Gérer vos adresses</a>
                </div>


            </div>
        </div>

        
    </div>
</div>
