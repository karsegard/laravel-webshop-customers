@php

$request_password_route = config('kda.webshop.routes.password.request_form');
$register_route = config('kda.webshop.routes.register');

$show_register_link = config('kda.webshop.routes.show_register_link');
@endphp
<form method="POST" action="{{ route(config('kda.webshop.routes.login')) }}">
    @csrf

    <div class="form-group row">
        <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

        <div class="col-md-6">
            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email"
                value="{{ old('email') }}" required autocomplete="email" autofocus>

            @error('email')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>

    <div class="form-group row">
        <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

        <div class="col-md-6">
            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror"
                name="password" required autocomplete="current-password">

            @error('password')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>

    <div class="form-group row">
        <div class="col-md-6 offset-md-4">
            <div class="form-check">
                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember')
                    ? 'checked' : '' }}>

                <label class="form-check-label" for="remember">
                    {{ __('Remember Me') }}
                </label>
            </div>
        </div>
    </div>

    <div class="form-group row mb-0">
        <div class="col-md-8 offset-md-4">
            <button type="submit" class="btn btn-primary">
                {{ __('Login') }}
            </button>

            @if (Route::has($request_password_route))
            <a class="btn btn-link" href="{{ route($request_password_route) }}">
                {{ __('Forgot Your Password?') }}
            </a>
            @endif
            
            @if ($show_register_link && Route::has($register_route) !=="")
            <a class="btn btn-link" href="{{ route($register_route) }}">
                {{ __('Vous n\'avez pas de compte ? ') }}
            </a>
            @endif
        </div>
    </div>
</form>