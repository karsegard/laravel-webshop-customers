<?php

return [
    'middleware' => [
        'web',
        \KDA\Shop\Customer\Http\Middleware\ForbidEmptyPasswordLogin::class
    ],
    
    'customers' => [
       
        'routes' => [
            'options' => [
                'login' => true,
                'register' => true,
                'dashboard' => true,
            ],
            
        ],
        'guard' => 'customers',
        'model'=> '\App\Models\CustomerUser',
        'register'=>[
            'validation'=>[]
        ]
    ],
    'routes' => [
        'dashboard'=> 'account',
        'prefix' => 'shop',
        'login' => 'login',
        'register' => 'register',
        'logout' => 'logout',
        'show_register_link'=> true,
        'password' => [
            'request_link' => 'password.email', 
            'request_form'=>'password.request',
            'reset' => 'password.update',
            'reset_token'=> 'password.reset'
        ]
    ],
    'views' => [
        'login' => 'kda/webshop::auth.login',
        'register' => 'kda/webshop::auth.register',
        'request_form' => 'kda/webshop::auth.passwords.request_form',
        'reset_form' => 'kda/webshop::auth.passwords.reset_form',
        'confirm' => 'kda/webshop::auth.passwords.confirm'
    ],

    
];
