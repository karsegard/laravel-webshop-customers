<?php

return [

    //generic options
    'table'=> 'customers',
    'model'=> '\App\Models\CustomerUser',
    'user_identifier_attribute'=> 'email',

    //register options
    'auto_login_after_register'=> true,
    'force_duplicate_email_check'=>true,
    'forced_duplicate_email_check_error'=>'Already exists',

    //views

    'view_verify' => 'kda/webshop::auth.verify',
    'view_request_form' => 'kda/webshop::auth.passwords.request_form'
];