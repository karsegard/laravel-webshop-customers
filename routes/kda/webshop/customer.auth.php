<?php


use Illuminate\Support\Facades\Route;

Route::group(
    [
        'namespace'  => 'KDA\Shop\Customer\Http\Controllers',
        'middleware' => config('kda.webshop.middleware', 'web'),
        'prefix'     => config('kda.webshop.routes.prefix'),
    ],
    function () {

        if (config('kda.webshop.customers.routes.options.login', true)) {
            Route::get('login', 'Auth\LoginController@showLoginForm')->name(config('kda.webshop.routes.login'));
            Route::post('login', 'Auth\LoginController@login');
            Route::get('logout', 'Auth\LoginController@logout')->name(config('kda.webshop.routes.logout'));
            Route::post('logout', 'Auth\LoginController@logout');
        }

        if (config('kda.webshop.customer.routes.options.register', true)) {
            Route::get('register', 'Auth\RegisterController@showRegistrationForm')
                ->name(config('kda.webshop.routes.register'));

            Route::post('register', 'Auth\RegisterController@register');
        }

        Route::get('password/request', 'Auth\ForgotPasswordController@showLinkRequestForm')
            ->name(config('kda.webshop.routes.password.request_form'));

        Route::post('password/request', 'Auth\ForgotPasswordController@sendResetLinkEmail')
            ->name(config('kda.webshop.routes.password.request_link'));

        Route::post('password/reset', 'Auth\ResetPasswordController@reset')
            ->name(config('kda.webshop.routes.password.reset'));
        Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')
            ->name(config('kda.webshop.routes.password.reset_token'));



        Route::get('email/verify', 'Auth\VerificationController@show')->name('verification.notice');
        Route::get('email/verify/{id}/{hash}', 'Auth\VerificationController@verify')->name('verification.verify');
        Route::post('email/resend', 'Auth\VerificationController@resend')->name('verification.resend');

       

        Route::get('password/confirm', 'Auth\ConfirmPasswordController@showConfirmForm')->name('password.confirm');
        Route::post('password/confirm', 'Auth\ConfirmPasswordController@confirm');


        if (config('kda.webshop.customers.routes.options.dashboard', true)) {

            Route::get(config('kda.webshop.routes.dashboard'),'DashboardController@index')->middleware('auth:' . kda_webshop_guard_name());
          
        }

    }
);