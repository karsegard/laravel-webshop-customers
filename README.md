# Webshop

## Customers Auth

    sail artisan vendor:publish --provider="KDA\Webshop\CustomerProvider" --tag="config" --force

### Install

Configure Laravel auth guard

To enable auth, you have to add **config/auth.php**

    'guards' => [
        ...
        'customers'=> [
            'driver' => 'session',
            'provider' => 'users' // <- change this if  you need another model
        ]
        ...
    ],

If you need to use another model

    'providers' => [
        ...
        'users' => [
            'driver' => 'eloquent',
            'model' => App\Models\Customer::class,
        ],
        ...

    ],

in your model (the default one or the custom one), you have to declare the guard

    class User extends Authenticatable
    {

        protected $guard_name = 'customers';

#### install the hook redirect middleware

in **app/http/Kernel.php**

add

    protected $routeMiddleware = [
        ....
        'hook'=> \KDA\Shop\Customer\Http\Middleware\HookAuthRedirect::class
        ...
    ];

adjust priority by adding

     protected $middlewarePriority = [
        \Illuminate\Session\Middleware\StartSession::class,
        \Illuminate\View\Middleware\ShareErrorsFromSession::class,
        \KDA\Shop\Customer\Http\Middleware\HookAuthRedirect::class,  // <-- right after Session
        // other middlewares
        \App\Http\Middleware\Authenticate::class,
        \Illuminate\Session\Middleware\AuthenticateSession::class,
        \Illuminate\Routing\Middleware\SubstituteBindings::class,
        \Illuminate\Auth\Middleware\Authorize::class,
    ];

## Orders

cf [laravel-shop-order](https://gitlab.com/karsegard/laravel-shop-order)

## Products

Wip

## Cart

cf [laravel-cart](https://gitlab.com/karsegard/laravel-cart)
