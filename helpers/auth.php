<?php


if (! function_exists('kda_webshop_guard_name')) {
    /*
     * Returns the name of the guard defined
     * by the application config
     */
    function kda_webshop_guard_name()
    {
        return config('kda.webshop.customers.guard', config('auth.defaults.guard'));
    }
}

if (! function_exists('kda_webshop_auth')) {
    /*
     * Returns the name of the guard defined
     * by the application config
     */
    function kda_webshop_auth()
    {
        return auth(kda_webshop_guard_name());
    }
}




if (! function_exists('kda_webshop_dashboard')) {
    /*
     * Returns the name of the guard defined
     * by the application config
     */
    function kda_webshop_dashboard()
    {
        return config('kda.webshop.routes.prefix').'/'.config('kda.webshop.routes.dashboard');
    }
}

if (! function_exists('webshop_hook_message')) {
    /*
     * Returns the name of the guard defined
     * by the application config
     */
    function webshop_hook_message()
    {
        return session('kda_hooks.'.kda_webshop_guard_name().'.message');
    }
}


if (! function_exists('webshop_user')) {
    /*
     * Returns back a user instance without
     * the admin guard, however allows you
     * to pass in a custom guard if you like.
     */
    function webshop_user()
    {
        return kda_webshop_auth()->user();
    }
}