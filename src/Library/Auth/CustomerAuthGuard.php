<?php

namespace KDA\Shop\Customer\Library\Auth;

use Illuminate\Support\Facades\Auth;
trait CustomerAuthGuard {

    protected function guard()
    {
        return Auth::guard(kda_webshop_guard_name());
    }
}