<?php

namespace KDA\Shop\Customer\Library\Auth;

trait CustomerRedirect {

    
    
    public function redirectTo(){
        $value = request()->session()->pull('kda_hooks.'.kda_webshop_guard_name().'.name');
        $parameters = request()->session()->pull('kda_hooks.'.kda_webshop_guard_name().'.parameters');
        request()->session()->pull('kda_hooks.'.kda_webshop_guard_name().'.message');
        if($value){
            return route($value,$parameters);
        }
        return kda_webshop_dashboard();
    }


}