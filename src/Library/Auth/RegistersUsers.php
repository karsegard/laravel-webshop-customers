<?php

namespace KDA\Shop\Customer\Library\Auth;

use Illuminate\Auth\Events\Registered;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\RedirectsUsers;
trait RegistersUsers
{
    use RedirectsUsers;
    use CustomerAuthGuard;
    /**
     * Show the application registration form.
     *
     * @return \Illuminate\View\View
     */


    public function showRegistrationForm()
    {
        if (!kda_webshop_auth()->check()) {

            return view(config('kda.webshop.views.register'));
        }
        return redirect(kda_webshop_dashboard());
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        if (config('kda.customers.auto_login_after_register', true)) {
            $this->guard()->login($user);
        }

        if ($response = $this->registered($request, $user)) {
            return $response;
        }

        return $request->wantsJson()
            ? new JsonResponse([], 201)
            : redirect($this->redirectPath());
    }


    /**
     * The user has been registered.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {
        //
    }
}
