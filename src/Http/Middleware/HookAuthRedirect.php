<?php

namespace KDA\Shop\Customer\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;

class HookAuthRedirect
{
    public function handle($request, Closure $next, $guard, $info = "")
    {
        // dd(kda_webshop_auth()->check(),request()->route()->getName());
        if (!kda_webshop_auth()->check()) {
            $parameters = request()->route()->parameters;
            $name = request()->route()->getName();
            \Session::put('kda_hooks.' . $guard . '.name', $name);
            \Session::put('kda_hooks.' . $guard . '.parameters', $parameters);
            \Session::put('kda_hooks.' . $guard . '.message', $info);
            \Session::save();
        }
        return $next($request);
    }
}
