<?php

namespace KDA\Shop\Customer\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;

class ForbidEmptyPasswordLogin
{

    protected $except = [
        '/shop/password/reset', // allow this URL to load even if "weak_password" exists
        // add more for your change-password CRUD URLs
    ];

    protected function inExceptArray($request)
    {
        foreach ($this->except as $except) {
            if ($except !== '/') {
                $except = trim($except, '/');
            }

            if ($request->fullUrlIs($except) || $request->is($except)) {
                return true;
            }
        }

        return false;
    }

    public function handle($request, Closure $next,...$guards)
    {
        if (kda_webshop_auth()->check() && webshop_user()) {
                if (webshop_user()->password === NULL) {
                    kda_webshop_auth()->logout();
                    return redirect("/shop");
                }
        }
        return $next($request);
    }
}
