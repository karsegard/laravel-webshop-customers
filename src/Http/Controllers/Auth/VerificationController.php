<?php

namespace KDA\Shop\Customer\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\VerifiesEmails;

class VerificationController extends Controller
{

    use \KDA\Shop\Customer\Library\Auth\CustomerAuthGuard;
    use \KDA\Shop\Customer\Library\Auth\CustomerRedirect;

    /*
    |--------------------------------------------------------------------------
    | Email Verification Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling email verification for any
    | user that recently registered with the application. Emails may also
    | be re-sent if the user didn't receive the original email message.
    |
    */

    use \KDA\Shop\Customer\Library\Auth\VerifiesEmails;

    /**
     * Where to redirect users after verification.
     *
     * @var string
     */
   // protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct( Request $request)
    {
        $this->middleware('auth:'.kda_webshop_guard_name());
        $this->middleware('signed')->only('verify');
        $this->middleware('throttle:6,1')->only('verify', 'resend');

    }
}
