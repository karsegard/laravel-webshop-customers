<?php

namespace KDA\Shop\Customer\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use KDA\Shop\Customer\Library\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers {showRegistrationForm as defaultshowRegistrationForm ;  }
    use \KDA\Shop\Customer\Library\Auth\CustomerRedirect;
    
   



    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {

        $additionnal_validator = config('kda.webshop.customers.register.validation',[]);
        $messages = config('kda.webshop.customers.register.messages',[]);
        $validator =  Validator::make($data, array_merge([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:'.config('kda.customers.table')],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            
        ],$additionnal_validator),$messages);

        $validator->after(function($validator) use ($data){
            $this->checkIfUserAttributeExists($data,$validator);
        });
        return $validator;
    }


    protected function checkIfUserAttributeExists($data,$validator){
        $user_attribute = config('kda.customers.user_identifier_attribute','email');

        $should_check_user = config('kda.customers.force_duplicate_email_check',true);
        $user = $data[$user_attribute];

        $model = config('kda.customers.model');
        if($should_check_user){
            if($model::where($user_attribute,$user)->first()){
                $validator->errors()->add($user_attribute, config('kda.customers.forced_duplicate_email_check_error'));
            }
        }

    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
       
        $model = config('kda.customers.model');
       
      //  $data['password']= Hash::make($data['password']);
        return $model::create($data);
    }

   

}
