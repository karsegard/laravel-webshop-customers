<?php

namespace KDA\Shop\Customer\Http\Controllers\Auth;


use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use KDA\Shop\Customer\Library\Auth\ResetsPasswords;
use Illuminate\Http\Request;
class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords {showResetForm as defaultshowResetForm ;}
    use \KDA\Shop\Customer\Library\Auth\CustomerRedirect;

    

    public function showResetForm(Request $request)
    {
        $token = $request->route()->parameter('token');

        return view(config('kda.webshop.views.reset_form'))->with(
            ['token' => $token, 'email' => $request->email]
        );
    }
}
