<?php

namespace KDA\Shop\Customer\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DashboardController extends Controller
{

    use \KDA\Shop\Customer\Library\Auth\CustomerAuthGuard;
    use \KDA\Shop\Customer\Library\Auth\CustomerRedirect;

    

    public function index(){
        return view('kda/webshop::dashboard');
    }

    public function createAddress(){

    }

    public function viewAddresses(){
        
    }
}
