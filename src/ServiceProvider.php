<?php

namespace KDA\Shop\Customer;

use KDA\Laravel\PackageServiceProvider;


class ServiceProvider extends PackageServiceProvider
{

    use \KDA\Laravel\Traits\HasMigration;
    use \KDA\Laravel\Traits\HasConfig;
    use \KDA\Laravel\Traits\HasViews;
    use \KDA\Laravel\Traits\HasRoutes;
    use \KDA\Laravel\Traits\HasHelper;
    use \KDA\Laravel\Traits\HasProviders;

    protected $additionnalProviders = [
        EventServiceProvider::class
    ];
    protected $routes = [
        '/kda/webshop/customer.auth.php'
    ];
   
    

    protected $configs = [
        'kda/webshop.php'=> 'kda.webshop',
        'kda/customers.php'=> 'kda.customers'
    ];

    protected $registerViews = 'kda/webshop';
    protected $publishViewsTo = 'vendor/kda/webshop';
     
 
    protected function packageBaseDir () {
        return dirname(__DIR__,1);
    }

    

    public function boot (){
        $this->loadHelper('auth.php');
        
        parent::boot();
    }
   
}
